package br.com.lyncas.GoogleBooksJSF.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

public class Book {
	
	private String id;
	private String etag;
	private String selfLink;
	private JsonObject volumeInfo;
	private JsonObject accessInfo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEtag() {
		return etag;
	}
	public void setEtag(String etag) {
		this.etag = etag;
	}
	public String getSelfLink() {
		return selfLink;
	}
	public void setSelfLink(String selfLink) {
		this.selfLink = selfLink;
	}
	public JsonObject getVolumeInfo() {
		return volumeInfo;
	}
	public void setVolumeInfo(JsonObject volumeInfo) {
		this.volumeInfo = volumeInfo;
	}
	public JsonObject getAccessInfo() {
		return accessInfo;
	}
	public void setAccessInfo(JsonObject accessInfo) {
		this.accessInfo = accessInfo;
	}
	public String getThumbnail() {
		if (this.volumeInfo != null) {
			JsonObject imageLinks = this.volumeInfo.getAsJsonObject("imageLinks");
			if (imageLinks != null) {
				return (imageLinks.has("thumbnail") ? imageLinks.get("thumbnail").getAsString() : 
					(imageLinks.has("smallThumbnail") ? imageLinks.get("smallThumbnail").getAsString() : ""));
			} else return "";
		}
		else return null;
	}
	public String getPreviewLink() {
		if (this.volumeInfo != null) return this.volumeInfo.get("previewLink").getAsString();
		else return null;
	}
	public String getAuthor() {
		if (this.volumeInfo != null) return this.volumeInfo.has("authors") ? this.volumeInfo.get("authors").toString().replace("[", "").replace("]", "").replace("\"", "") : "";
		else return null;
	}
	public String getLancamento() {
		String lancamento = "";
		if (this.volumeInfo != null) {
			try {
				lancamento = this.volumeInfo.has("publishedDate") ? this.volumeInfo.get("publishedDate").getAsString() : "";
				Date lancDate = new SimpleDateFormat("yyyy-MM-dd").parse(lancamento);
				lancamento = new SimpleDateFormat("dd/MM/yyyy").format(lancDate);
			} catch (ParseException e) {
				if (!e.getClass().getName().equals("java.text.ParseException")) {
					e.printStackTrace();
					lancamento = "";
				}
			}
		}
		return lancamento;
	}
	public String getTitle() {
		if (this.volumeInfo != null) return this.volumeInfo.get("title").getAsString();
		else return null;
	}
	
	public String getDescription() {
		if (this.volumeInfo != null) return this.volumeInfo.get("description").getAsString();
		else return null;
	}
}
