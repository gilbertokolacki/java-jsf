package br.com.lyncas.GoogleBooksJSF.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.menu.MenuModel;

import com.google.gson.Gson;

import br.com.lyncas.GoogleBooksJSF.model.Book;
import br.com.lyncas.GoogleBooksJSF.model.Dados;

@SessionScoped
@ManagedBean(name="menuBean")
public class MenuBean {
	
	private static String URL = "https://www.googleapis.com/books/v1/volumes?q=";
	private MenuModel model;
	private String search;
	private List<Book> books = new ArrayList<Book>();
	private Book selectedBook;
	private List<Book> booksFavorites = new ArrayList<Book>();
	
	public MenuModel getModel() {
        return model;
    }
	
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void searchBooks() {
		try {
			this.books = new ArrayList<Book>();
			Gson gson = new Gson();
            HttpURLConnection conn = (HttpURLConnection) new URL(URL.concat(this.getSearch())).openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept-Charset", "UTF-8"); 
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) System.out.println("Erro " + conn.getResponseCode() + " ao obter dados da URL " + URL);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String output = "";
            String line;
            while ((line = br.readLine()) != null) {
                output += line;
            }
            conn.disconnect();

            Dados dados = gson.fromJson(new String(output.getBytes()), Dados.class);
            this.books = dados.getItems();
        } catch (IOException ex) {
        	System.out.println(ex);
        }
    }
    
    public void favoritar(Book book) {
    	if (this.verificaBookAdd(book.getId())) {
    		this.booksFavorites.add(book);
    		this.addMessage("Favoritado", "Favoritado");
    	} else {
    		this.addMessage("Livro ja esta nos favoritos", "");
    	}
    }
    
    public void remover(Book bookRemove) {
    	this.booksFavorites.remove(bookRemove);
    	this.addMessage("Removido!", "Removido");
    }
    
    private boolean verificaBookAdd(String idBook) {
    	boolean adicionar = true;
    	for (Book bookFavorito : this.booksFavorites) {
			if (bookFavorito.getId().equals(idBook)) {
				adicionar = false;
				break;
			}
		}
    	return adicionar;
    }

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book getSelectedBook() {
		return selectedBook;
	}

	public void setSelectedBook(Book selectedBook) {
		this.selectedBook = selectedBook;
	}

	public List<Book> getBooksFavorites() {
		return booksFavorites;
	}

	public void setBooksFavorites(List<Book> booksFavorites) {
		this.booksFavorites = booksFavorites;
	}
}
